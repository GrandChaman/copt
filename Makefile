# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: fle-roy <francis.leroy@protonmail.ch>      +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2018/01/16 13:28:08 by fle-roy           #+#    #+#              #
#    Updated: 2019/09/27 17:38:46 by fle-roy          ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

vpath %.c src
vpath %.c src/utils
vpath %.c src/llist
vpath %.c tests

SRC = \
		copt.c \
		copt_str_utils.c \
		copt_llist_new.c \
		copt_llist_del.c \
		copt_llist_destroy.c \
		copt_llist_push_back.c \
		copt_llist_push_front.c \
		copt_llist_iter.c \
		copt_free_parsed.c \
		copt_malloc.c \
		copt_cmd.c \
		copt_pipeline.c \
		copt_pipeline_utils.c \
		copt_parsed.c \
		copt_opt_utils.c \
		copt_opt.c \
		copt_opt_error.c \
		copt_opt_user_utils.c \
		copt_help.c
INCLUDE= include
OBJ_DIR=obj
DEP_DIR=dep
CFLAG = -g3 -Wall -Wextra -Werror -I $(INCLUDE)
CC = clang
LN = ar
LFLAG = rsc
TEST_CFLAG = -I $(INCLUDE)
TEST_LFLAG = -lcriterion
TEST_BIN = copt_test
TEST_SRC = \
			utils.test.c \
			llist.test.c \
			cmd.test.c \
			long_opt.test.c \
			short_opt.test.c \
			user_utils.test.c \
			global.test.c
OBJ = $(SRC:%.c=$(OBJ_DIR)/%.o)
DEP = $(SRC:%.c=$(DEP_DIR)/%.d)
OBJ_TEST = $(TEST_SRC:%.test.c=$(OBJ_DIR)/%.test.o)
DEP_TEST = $(TEST_SRC:%.test.c=$(DEP_DIR)/%.test.d)
TEST_BIN = copt_test
ifeq ($(DEV),true)
	CFLAG += -g3 -DDEV
	TEST_CFLAG += -g3 -DDEV
endif

ifeq ($(CODE_COVERAGE),true)
	CFLAG += -fprofile-arcs -ftest-coverage
	TEST_CFLAG += -fprofile-arcs -ftest-coverage
	TEST_LFLAG += -fprofile-arcs -ftest-coverage
endif
NAME = libcopt.a
NAME_UP = COPT
all: $(NAME)
test: $(TEST_BIN)
$(OBJ_DIR)/%.test.o: %.test.c
	@printf "\r\033[K[$(NAME_UP)] \033[1;32mBuilding $<\033[0m"
	@$(CC) $(TEST_CFLAG) -c $< -o $@
$(DEP_DIR)/%.test.d: %.test.c
	@printf "\r\033[K[$(NAME_UP)] \033[1;32mGenerating dependencies - $<\033[0m"
	@$(CC) $(TEST_CFLAG) -MM $^ | sed -e '1s/^/$(OBJ_DIR)\//' > $@
$(OBJ_DIR)/%.o: %.c
	@printf "\r\033[K[$(NAME_UP)] \033[1;32mBuilding $<\033[0m"
	@$(CC) $(CFLAG) -c $< -o $@
$(DEP_DIR)/%.d: %.c
	@printf "\r\033[K[$(NAME_UP)] \033[1;32mGenerating dependencies - $<\033[0m"
	@$(CC) $(CFLAG) -MM $^ | sed -e '1s/^/$(OBJ_DIR)\//' > $@
$(TEST_BIN): $(NAME) $(OBJ_TEST)
	@$(CC) $(TEST_CFLAG) $(NAME) $(TEST_LFLAG) $(OBJ_TEST) -o $(TEST_BIN)
	@printf "\r\033[K[$(NAME_UP)] \033[1;32mTests done!\033[0m\n"
$(NAME): $(OBJ)
	@printf "\r\033[K[$(NAME_UP)] \033[1;32mLinking...\033[0m"
	@$(LN) $(LFLAG) $(NAME) $(OBJ) 
	@printf "\r\033[K[$(NAME_UP)] \033[1;32mDone!\033[0m\n"
clean:
	@rm -f $(OBJ) $(OBJ_TEST)
	@printf "[$(NAME_UP)] \033[1;31mCleaned .o!\033[0m\n"
dclean:
	@rm -f $(DEP)
	@printf "[$(NAME_UP)] \033[1;31mCleaned .d!\033[0m\n"
fclean: clean
	@rm -f $(NAME) $(TEST_BIN)
	@printf "[$(NAME_UP)] \033[1;31mCleaned .a!\033[0m\n"
re:
	@$(MAKE) fclean
	@$(MAKE) all
-include $(DEP)
-include $(DEP_TEST)
.PHONY: all clean fclean re dclean test
