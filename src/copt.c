/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   copt.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fle-roy <francis.leroy@protonmail.ch>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/05 14:34:08 by fle-roy           #+#    #+#             */
/*   Updated: 2020/02/27 18:29:09 by fle-roy          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "copt.h"
#include "copt_internal.h"
#include <stdio.h>

uint8_t		copt_alloc_args(t_copt_args *args, size_t argc, const char **argv)
{
	size_t			i;

	i = -1;
	args->argc = argc;
	if (!(args->argv = copt_malloc(sizeof(char*) * (argc + 1))))
		return (1);
	while (++i < argc)
		if (!(args->argv[i] = (const char*)copt_strsub((char*)argv[i], 0, 0)))
			return (1);
	return (0);
}

char		copt_exec(t_copt copt, t_copt_parsed *res)
{
	size_t			ret;

	ret = 0;
	if (res->gcmd)
		ret = res->gcmd->cb(res);
	if (res->cmd && res->cmd->cb)
		return (res->cmd->cb(res));
	if (res->gcmd)
		return (ret);
	ret |= -1;
	if (!res->cmd || !res->cmd->cb)
		copt.print("Error: No command provided\n");
	else
		copt.print("Error: Command not found\n");
	copt_display_help(copt);
	return (ret);
}

char		copt_load(t_copt copt, t_copt_parsed *res, size_t argc,
	const char **argv)
{
	t_copt_args		args;
	char			ret;

	ret = 0;
	if (copt_alloc_args(&args, argc, argv) ||
		copt_parsed_init(copt, res, &args))
		return (1);
	if ((ret = copt_pipeline_routine(copt, res, &args)))
	{
		copt_free_parsed(res);
		return (ret);
	}
	ret = copt_exec(copt, res);
	return (ret);
}
