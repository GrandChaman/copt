/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   copt_parsed.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fle-roy <francis.leroy@protonmail.ch>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/05 14:34:08 by fle-roy           #+#    #+#             */
/*   Updated: 2019/09/27 17:26:21 by fle-roy          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "copt.h"
#include "copt_internal.h"
#include <stdio.h>

t_copt_parsed_opt		*copt_init_parsed_opt(t_copt_opt *opt)
{
	t_copt_parsed_opt	*nopt;

	if (!(nopt = copt_malloc(sizeof(t_copt_parsed_opt))))
		return (NULL);
	nopt->argc = opt->argc;
	if (!(nopt->argv = copt_malloc(sizeof(char*) * (opt->argc + 1))))
		return (NULL);
	nopt->id = opt->id;
	nopt->times = 1;
	return (nopt);
}

t_copt_parsed_opt		**copt_init_parsed_opts(t_copt_cmd *cmd)
{
	size_t				i;
	t_copt_parsed_opt	**opts;
	size_t				opt_nb;

	i = 0;
	if (!cmd || !cmd->opt || !*cmd->opt)
		return (NULL);
	opt_nb = copt_opt_nb_in_cmd(cmd);
	if (!(opts = copt_malloc(sizeof(t_copt_parsed_opt*)
		* (opt_nb + 1))))
		return (NULL);
	while (cmd->opt[i])
	{
		opts[i] = NULL;
		i++;
	}
	return (opts);
}

char					copt_parsed_init(t_copt copt, t_copt_parsed *res,
	t_copt_args *args)
{
	t_copt_cmd	*cmd;

	res->argc = args->argc;
	res->argv = args->argv;
	res->gopts = NULL;
	res->opts = NULL;
	res->cmd = NULL;
	res->gcmd = NULL;
	res->params = NULL;
	res->list = NULL;
	if (copt.cmd && (cmd = copt_find_cmd(copt, NULL)))
	{
		res->gcmd = cmd;
		res->gopts = copt_init_parsed_opts(cmd);
	}
	return (0);
}
