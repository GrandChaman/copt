/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   copt_help.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fle-roy <francis.leroy@protonmail.ch>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/13 11:48:12 by fle-roy           #+#    #+#             */
/*   Updated: 2019/09/23 15:00:56 by fle-roy          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "copt.h"
#include "copt_internal.h"

size_t		copt_help_get_max_opts_size(t_copt copt)
{
	size_t	long_opt_size;
	size_t	i;
	size_t	ii;
	size_t	tmp;

	i = 0;
	ii = 0;
	long_opt_size = 0;
	while (copt.cmd && copt.cmd[i])
	{
		ii = 0;
		if (copt.cmd[i]->opt)
			while (copt.cmd[i]->opt[ii])
			{
				if ((tmp = copt_strlen(copt.cmd[i]->opt[ii]->lname))
					> long_opt_size)
					long_opt_size = tmp;
				ii++;
			}
		i++;
	}
	return (long_opt_size + 16);
}

void		copt_display_help_opt(t_copt copt, t_copt_opt *opt, size_t lsize)
{
	lsize -= 6;
	if (opt->argc)
		lsize -= 6;
	if (opt->lname && opt->sname)
		copt.print("\t-%c,\t--%-*s", opt->sname, lsize, opt->lname);
	else if (opt->sname)
		copt.print("\t-%c\t\t", opt->sname);
	else if (opt->lname)
		copt.print("\t\t--%-*s", lsize, opt->lname);
	if (opt->argc)
		copt.print("(% 3d) ", opt->argc);
	if (opt->mandatory)
		copt.print(" (M) ");
	else
		copt.print(" ( ) ");
	if (opt->desc)
		copt.print("%-*s%s\n", 10, "", opt->desc);
	else
		copt.print("\n");
}

void		copt_display_help_cmd(t_copt copt, t_copt_cmd *cmd, size_t lsize)
{
	size_t	i;

	i = 0;
	if (cmd->text)
		copt.print("Command: %s\n", cmd->text);
	else
		copt.print("Global command:\n");
	if (cmd->desc)
		copt.print("%s\n", cmd->desc);
	copt.print("\tShort\t%-*s%-*s Mandatory\n", lsize - 10, "Long", 6, "Args");
	while (cmd->opt && cmd->opt[i])
		copt_display_help_opt(copt, cmd->opt[i++], lsize);
}

void		copt_display_help(t_copt copt)
{
	size_t		i;
	size_t		max_lsize;

	i = 0;
	max_lsize = copt_help_get_max_opts_size(copt);
	copt.print("%s: [global opts] <cmd> [opts] [args]\nVersion : %s\n",
		copt.prgm_name, copt.version);
	if (copt.desc)
		copt.print("%s\n", copt.desc);
	while (copt.cmd && copt.cmd[i])
		copt_display_help_cmd(copt, copt.cmd[i++], max_lsize);
}
