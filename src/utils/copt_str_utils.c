/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   copt_str_utils.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fle-roy <francis.leroy@protonmail.ch>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/05 14:45:35 by fle-roy           #+#    #+#             */
/*   Updated: 2019/09/27 15:45:25 by fle-roy          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "copt_internal.h"

size_t		copt_strlen(char *str)
{
	unsigned int i;

	i = 0;
	if (!str)
		return (0);
	while (str[i])
		i++;
	return (i);
}

char		*copt_strsub(char *s, size_t start, size_t len)
{
	char			*res;
	unsigned int	i;

	i = -1;
	if (!s)
		return (NULL);
	if (!len)
		len = copt_strlen(s);
	if (!(res = copt_malloc(sizeof(char) * (len + 1))))
		return (NULL);
	while (++i < len)
		res[i] = s[start + i];
	res[i] = '\0';
	return (res);
}

char		copt_is_in_opt_charset(const char *s)
{
	if (!s)
		return (0);
	while (*s)
		if (*s < 42 || *s > 126)
			return (0);
		else
			s++;
	return (1);
}

char		copt_strcmp(const char *lhs, const char *rhs)
{
	if (lhs && rhs)
		while (*lhs && *rhs && *lhs == *rhs)
		{
			lhs++;
			rhs++;
		}
	if (!lhs || !rhs)
		return (lhs == rhs ? 0 : 1);
	return ((unsigned char)*lhs - (unsigned char)*rhs);
}
