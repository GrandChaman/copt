/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   copt_malloc.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fle-roy <francis.leroy@protonmail.ch>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/05 14:45:35 by fle-roy           #+#    #+#             */
/*   Updated: 2019/12/09 17:37:16 by fle-roy          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "copt_internal.h"
#define MALLOC_FAILED_ERROR "Malloc failed, aborting"

void	*copt_malloc(size_t len)
{
	void	*res;
	size_t	i;

	res = malloc(len);
	if (!res)
	{
		write(2, MALLOC_FAILED_ERROR, sizeof(MALLOC_FAILED_ERROR) - 1);
		return (NULL);
	}
	i = -1;
	while (++i < len)
		*((char*)res + i) = 0;
	return (res);
}

void	*copt_memcpy(void *dest, void *src, size_t count)
{
	void	*bckup;

	bckup = dest;
	while (count--)
		*((unsigned char*)dest++) = *((unsigned char*)src++);
	return (bckup);
}

void	*copt_memset(void *dest, unsigned char c, size_t count)
{
	void	*bckup;

	bckup = dest;
	while (count--)
		*((unsigned char*)dest++) = c;
	return (bckup);
}

void	*copt_realloc(void *old, size_t oldlen, size_t len)
{
	void *new;

	if (!(new = copt_malloc(len)))
		return (NULL);
	if (old)
	{
		copt_memcpy(new, old, oldlen);
		free(old);
	}
	return (new);
}

void	copt_free(void **ptr)
{
	free(*ptr);
	*ptr = NULL;
}
