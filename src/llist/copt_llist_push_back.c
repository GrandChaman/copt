/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   copt_llist_push_back.c                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fle-roy <francis.leroy@protonmail.ch>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/07 17:09:32 by fle-roy           #+#    #+#             */
/*   Updated: 2019/09/27 15:42:18 by fle-roy          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "copt_internal.h"
#include "copt.h"

uint8_t	copt_llist_push_back(t_copt_llist **begin_list, t_copt_cmd *cmd,
	t_copt_parsed_opt *opt, size_t index)
{
	t_copt_llist	*tmp;
	t_copt_llist	*cursor;

	if (begin_list == NULL)
		return (0);
	if (!(tmp = copt_llist_new(cmd, opt, index)))
		return (1);
	if (*begin_list == NULL)
	{
		*begin_list = tmp;
		return (0);
	}
	cursor = *begin_list;
	while (cursor->next)
		cursor = cursor->next;
	cursor->next = tmp;
	tmp->prev = cursor;
	return (0);
}
