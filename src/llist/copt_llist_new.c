/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   copt_llist_new.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fle-roy <francis.leroy@protonmail.ch>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/31 17:08:48 by bluff             #+#    #+#             */
/*   Updated: 2019/09/27 15:29:04 by fle-roy          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "copt.h"
#include "copt_internal.h"

t_copt_llist		*copt_llist_new(t_copt_cmd *cmd, t_copt_parsed_opt *opt,
	size_t index)
{
	t_copt_llist	*el;

	if (!(el = copt_malloc(sizeof(t_copt_llist))))
		return (NULL);
	el->cmd = cmd;
	el->opt = opt;
	el->next = NULL;
	el->prev = NULL;
	el->index = index;
	return (el);
}
