/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   copt_llist_del.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fle-roy <francis.leroy@protonmail.ch>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/31 17:29:58 by bluff             #+#    #+#             */
/*   Updated: 2019/05/16 12:00:07 by fle-roy          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "copt.h"
#include "copt_internal.h"

void	copt_llist_del(t_copt_llist **alst)
{
	if (!alst || !*alst)
		return ;
	if ((*alst)->next)
		(*alst)->next->prev = (*alst)->prev;
	if ((*alst)->prev)
		(*alst)->prev->next = (*alst)->next;
	copt_free((void**)alst);
}
