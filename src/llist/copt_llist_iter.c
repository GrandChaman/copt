/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   copt_llist_iter.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fle-roy <francis.leroy@protonmail.ch>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/31 17:51:16 by bluff             #+#    #+#             */
/*   Updated: 2019/05/16 14:13:26 by fle-roy          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "copt.h"
#include "copt_internal.h"

char	copt_llist_iter(t_copt copt, t_copt_llist *lst,
	char (*f)(t_copt, t_copt_llist *lst))
{
	char res;

	res = 0;
	while (lst)
	{
		if ((res = f(copt, lst)))
			return (res);
		lst = lst->next;
	}
	return (res);
}
