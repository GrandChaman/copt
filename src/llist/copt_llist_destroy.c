/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   copt_llist_destroy.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fle-roy <francis.leroy@protonmail.ch>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/22 13:21:52 by fle-roy           #+#    #+#             */
/*   Updated: 2019/05/16 12:04:33 by fle-roy          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "copt.h"
#include "copt_internal.h"

void		copt_llist_destroy(t_copt_llist **list)
{
	t_copt_llist *next;

	if (!list || !*list)
		return ;
	while (*list)
	{
		next = (*list)->next;
		copt_llist_del(list);
		*list = next;
	}
}
