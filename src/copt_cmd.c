/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   copt_cmd.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fle-roy <francis.leroy@protonmail.ch>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/05 14:34:08 by fle-roy           #+#    #+#             */
/*   Updated: 2019/05/22 14:04:49 by fle-roy          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "copt.h"
#include "copt_internal.h"

t_copt_cmd		*copt_find_cmd(t_copt copt, char *search)
{
	size_t		i;

	i = 0;
	while (copt.cmd[i])
		if (!copt_strcmp(copt.cmd[i]->text, search))
			return (copt.cmd[i]);
		else
			i++;
	return (NULL);
}
