/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   copt_free_parsed.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fle-roy <francis.leroy@protonmail.ch>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/21 15:31:03 by fle-roy           #+#    #+#             */
/*   Updated: 2019/07/01 11:19:02 by fle-roy          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "copt.h"
#include "copt_internal.h"

void		copt_free_opts(t_copt_cmd *cmd, t_copt_parsed_opt **opts)
{
	size_t	tmp;
	size_t	i;

	tmp = copt_opt_nb_in_cmd(cmd);
	i = 0;
	while (i < tmp)
	{
		if (opts[i])
			copt_free((void**)&opts[i]->argv);
		copt_free((void**)&opts[i++]);
	}
	copt_free((void**)&opts);
}

void		copt_free_parsed(t_copt_parsed *parsed)
{
	size_t	i;

	copt_llist_destroy(&parsed->list);
	if (parsed->gcmd)
		copt_free_opts(parsed->gcmd, parsed->gopts);
	parsed->gcmd = NULL;
	if (parsed->cmd)
		copt_free_opts(parsed->cmd, parsed->opts);
	parsed->cmd = NULL;
	if (parsed->params)
	{
		copt_free((void**)&parsed->params->argv);
		copt_free((void**)&parsed->params);
	}
	i = 0;
	while (i < parsed->argc)
		copt_free((void**)&parsed->argv[i++]);
	copt_free((void**)&parsed->argv);
	parsed->argc = 0;
}
