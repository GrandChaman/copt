/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   copt_opt_error.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fle-roy <francis.leroy@protonmail.ch>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/16 10:20:20 by fle-roy           #+#    #+#             */
/*   Updated: 2019/05/22 13:20:13 by fle-roy          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "copt.h"
#include "copt_internal.h"

char			copt_opt_error_validation(t_copt copt, t_copt_opt *opt)
{
	copt.print("Parameter(s) for option '%s' didn't pass "
			"validation\n", opt->lname);
	return (1);
}

char			copt_opt_error_args(t_copt copt, t_copt_opt *opt)
{
	copt.print("Not enough arguments for option : %s, needed %u Argument(s)\n",
		opt->lname, opt->argc);
	return (1);
}

char			copt_opt_doesnt_exists(t_copt copt, char *opt, t_copt_cmd *cmd)
{
	copt.print("Option %s doesn't exist for %s command\n",
		opt, cmd->text ? cmd->text : "global");
	return (1);
}
