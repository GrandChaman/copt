/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   copt_pipeline.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fle-roy <francis.leroy@protonmail.ch>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/05 14:34:08 by fle-roy           #+#    #+#             */
/*   Updated: 2019/09/27 15:28:33 by fle-roy          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "copt.h"
#include "copt_internal.h"

uint8_t				copt_load_g_params(t_copt_parsed *res,
	t_copt_args *args)
{
	size_t				i;
	char				*tmp;

	i = -1;
	if (!(res->params = copt_malloc(sizeof(t_copt_parsed_opt))))
		return (1);
	res->params->times = 1;
	if (copt_is_ddash((char*)args->argv[0]))
		copt_consume_arg(args);
	res->params->argc = args->argc;
	if (!(res->params->argv = copt_malloc(sizeof(char*) * (args->argc + 1))))
		return (1);
	while (args->argv[0] && ++i < res->params->argc)
	{
		tmp = copt_consume_arg(args);
		res->params->argv[i] = tmp;
	}
	return (0);
}

char				copt_pipeline_option(t_copt copt, t_copt_parsed *res,
	t_copt_args *args, char *ret)
{
	char				*tmp;
	char				lret;

	lret = 0;
	if ((tmp = copt_is_short_opt(args->argv[0])))
	{
		*ret = copt_read_short_opt(copt, res, args, tmp);
		lret = 1;
	}
	else if ((tmp = copt_is_long_opt(args->argv[0])))
	{
		*ret = copt_read_long_opt(copt, res, args, tmp);
		lret = 1;
	}
	return (lret);
}

char				copt_pipeline_routine(t_copt copt, t_copt_parsed *res,
	t_copt_args *args)
{
	t_copt_cmd			*cmd;
	size_t				i;
	char				ret;

	i = -1;
	ret = 0;
	while (args->argc && !ret)
	{
		++i;
		if (copt_pipeline_option(copt, res, args, &ret))
			continue ;
		else if (!res->cmd && copt.cmd
			&& (cmd = copt_find_cmd(copt, (char*)args->argv[0])))
		{
			copt_consume_arg(args);
			res->cmd = cmd;
			res->opts = copt_init_parsed_opts(cmd);
		}
		else if (i != 0)
			ret = copt_load_g_params(res, args);
		else
			copt_consume_arg(args);
	}
	return (ret);
}
