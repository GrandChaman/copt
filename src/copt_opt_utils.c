/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   copt_opt_utils.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fle-roy <francis.leroy@protonmail.ch>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/05 14:34:08 by fle-roy           #+#    #+#             */
/*   Updated: 2019/12/03 17:15:50 by fle-roy          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "copt.h"
#include "copt_internal.h"

t_copt_opt			*copt_find_short_opt(t_copt_cmd *cmd, char search)
{
	size_t		i;
	t_copt_opt	*res;

	i = 0;
	if (!cmd->opt)
		return (NULL);
	res = cmd->opt[i];
	while (res && res->sname)
	{
		if (res->sname == search)
		{
			res->id = i;
			return (res);
		}
		res = cmd->opt[++i];
	}
	return (NULL);
}

t_copt_opt			*copt_find_long_opt(t_copt_cmd *cmd, char *search)
{
	size_t		i;
	t_copt_opt	*res;

	i = 0;
	if (!cmd->opt)
		return (NULL);
	res = cmd->opt[i];
	if (search)
		while (res && res->lname)
		{
			if (!copt_strcmp(search, res->lname))
			{
				res->id = i;
				return (res);
			}
			res = cmd->opt[++i];
		}
	return (NULL);
}

size_t				copt_opt_nb_in_cmd(t_copt_cmd *cmd)
{
	size_t		i;

	i = 0;
	if (!cmd || !cmd->opt || !*cmd->opt)
		return (i);
	while (cmd->opt[i])
		i++;
	return (i);
}

uint8_t				copt_enlarge_parsed_opt(t_copt_parsed_opt *opt)
{
	opt->times++;
	if (!(opt->argv = copt_realloc(opt->argv,
	((opt->times - 1) * opt->argc * sizeof(char*)),
		((opt->times) * opt->argc * sizeof(char*)))))
		return (1);
	return (0);
}

char				copt_is_ddash(char *str)
{
	return (!copt_strcmp(str, "--"));
}
