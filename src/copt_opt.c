/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   copt_opt.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fle-roy <francis.leroy@protonmail.ch>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/05 14:34:08 by fle-roy           #+#    #+#             */
/*   Updated: 2019/05/22 13:30:12 by fle-roy          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "copt.h"
#include "copt_internal.h"

char					copt_read_short_opt(t_copt copt, t_copt_parsed *res,
	t_copt_args *args, char *opt)
{
	size_t				tmp;
	t_copt_cmd			*cmd;
	t_copt_parsed_opt	**po;
	t_copt_opt			*tmp_opt;

	tmp = 0;
	cmd = (res->cmd ? res->cmd : res->gcmd);
	po = (res->cmd ? res->opts : res->gopts);
	copt_consume_arg(args);
	while (opt && *opt)
	{
		if (!(tmp_opt = copt_find_short_opt(cmd, *opt)))
			return (copt_opt_doesnt_exists(copt, (char[]){*opt, '\0'}, cmd));
		if ((tmp = copt_insert_opt(res, cmd, po, tmp_opt)))
			return (tmp);
		if ((tmp = copt_opt_read_args(po[tmp_opt->id], &opt, args)))
			return (copt_opt_error_args(copt, tmp_opt));
		if ((tmp = copt_opt_validation(po, tmp_opt)))
			return (copt_opt_error_validation(copt, tmp_opt));
		if (!opt)
			break ;
		opt++;
	}
	return (0);
}

char					copt_read_long_opt(t_copt copt, t_copt_parsed *res,
	t_copt_args *args, char *opt)
{
	size_t				tmp;
	t_copt_cmd			*cmd;
	t_copt_parsed_opt	**po;
	t_copt_opt			*tmp_opt;

	tmp = 0;
	cmd = (res->cmd ? res->cmd : res->gcmd);
	po = (res->cmd ? res->opts : res->gopts);
	copt_consume_arg(args);
	if (!(tmp_opt = copt_find_long_opt(cmd, opt)))
		return (copt_opt_doesnt_exists(copt, opt, cmd));
	if ((tmp = copt_insert_opt(res, cmd, po, tmp_opt)))
		return (tmp);
	if ((tmp = copt_opt_read_args(po[tmp_opt->id], NULL, args)))
		return (copt_opt_error_args(copt, tmp_opt));
	if ((tmp = copt_opt_validation(po, tmp_opt)))
		return (copt_opt_error_validation(copt, tmp_opt));
	return (0);
}

char					*copt_is_short_opt(const char *opt)
{
	size_t	i;

	i = 0;
	while (opt && (opt[i] == ' ' || opt[i] == '\t'
		|| opt[i] == '\r' || opt[i] == '\n' || opt[i] == '\v'))
		i++;
	if (opt && opt[i] == '-' && opt[i + 1] != '-')
		if (copt_is_in_opt_charset((char*)opt + i + 1))
			return ((char*)opt + i + 1);
	return (NULL);
}

char					*copt_is_long_opt(const char *opt)
{
	size_t	i;

	i = 0;
	while (opt && (opt[i] == ' ' || opt[i] == '\t'
		|| opt[i] == '\r' || opt[i] == '\n' || opt[i] == '\v'))
		i++;
	if (opt && opt[i] == '-' && opt[i + 1] == '-' && opt[i + 2] != '\0')
		if (copt_is_in_opt_charset((char*)opt + i + 2))
			return ((char*)opt + i + 2);
	return (NULL);
}
