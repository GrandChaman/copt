/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   copt_pipeline_utils.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fle-roy <francis.leroy@protonmail.ch>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/05 14:34:08 by fle-roy           #+#    #+#             */
/*   Updated: 2020/02/27 18:28:35 by fle-roy          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "copt.h"
#include "copt_internal.h"

char			*copt_consume_arg(t_copt_args *args)
{
	char *res;

	res = (char*)args->argv[0];
	args->argv++;
	args->argc--;
	return (res);
}

char			*copt_restore_arg(t_copt_args *args)
{
	char *res;

	(args->argv)--;
	res = (char*)args->argv[0];
	args->argc++;
	return (res);
}

char			copt_opt_validation(t_copt_parsed_opt **res,
	t_copt_opt *opt)
{
	if (res[opt->id]->argc && opt->validation
		&& opt->validation(res[opt->id]->argc,
		&res[opt->id]->argv[(res[opt->id]->times - 1) * res[opt->id]->argc]))
		return (1);
	return (0);
}

char			copt_opt_read_args(t_copt_parsed_opt *res,
	char **actual, t_copt_args *args)
{
	size_t				i;
	char				*tmp;

	i = 0;
	if (res->argc && actual && *actual && *(*actual + 1))
	{
		res->argv[((res->times - 1)
				* res->argc) + i++] = *actual + 1;
		*actual = NULL;
	}
	while (i < res->argc)
	{
		tmp = copt_consume_arg(args);
		if (!tmp || copt_is_ddash(tmp))
			return (1);
		res->argv[((res->times - 1)
			* res->argc) + i] = tmp;
		i++;
	}
	return (0);
}

char			copt_insert_opt(t_copt_parsed *res, t_copt_cmd *cmd,
	t_copt_parsed_opt **po, t_copt_opt *opt)
{
	if (!opt)
		return (0);
	if (!po[opt->id])
		po[opt->id] = copt_init_parsed_opt(opt);
	else if (copt_enlarge_parsed_opt(po[opt->id]))
		return (1);
	return (copt_llist_push_back(&res->list,
		cmd, po[opt->id], po[opt->id]->times - 1));
}
