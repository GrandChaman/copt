/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   copt_opt_user_utils.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fle-roy <francis.leroy@protonmail.ch>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/31 13:32:19 by fle-roy           #+#    #+#             */
/*   Updated: 2019/07/01 09:19:58 by fle-roy          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "copt.h"
#include "copt_internal.h"

t_copt_parsed_opt	copt_get_last_opt(t_copt_parsed *parsed, size_t opt)
{
	t_copt_parsed_opt res;
	t_copt_parsed_opt *tmp;

	tmp = parsed->opts[opt];
	res.argc = tmp->argc;
	res.id = opt;
	res.times = tmp->times;
	res.argv = tmp->argv + (tmp->times - 1) * tmp->argc;
	return (res);
}

size_t				copt_get_mode(t_copt_parsed *parsed,
	size_t opt1, size_t opt2, size_t dft)
{
	t_copt_llist	*tmp;
	size_t			res;

	res = dft;
	tmp = parsed->list;
	while (tmp)
	{
		if (tmp->opt->id == opt1 || tmp->opt->id == opt2)
			res = tmp->opt->id;
		tmp = tmp->next;
	}
	return (res);
}
