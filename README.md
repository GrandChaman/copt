![pipeline](https://gitlab.com/GrandChaman/copt/badges/master/build.svg "Build status")
![coverage](https://gitlab.com/GrandChaman/copt/badges/master/coverage.svg "Code coverage")

# Building

Simply build using the Makefile default function : `make`

This project compile on a Mac OS X Mojave 10.14.6 using `Apple LLVM version 10.0.1 (clang-1001.0.46.4)`

## Testing

Simply build the tests using the Makefile default function : `make test`

and then run `./copt_test`

# Usage

## Configuration

This librairie exposes multiple structures that you can explore in [this file](include/copt.h#L17)

## Methods

This librairie exposes multiple methods that you can explore in [this file](include/copt.h#L82)