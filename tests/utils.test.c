/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   utils.test.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fle-roy <francis.leroy@protonmail.ch>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/26 14:57:51 by fle-roy           #+#    #+#             */
/*   Updated: 2019/05/14 13:45:22 by fle-roy          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "copt.test.h"

char	cb(t_copt_parsed *opt)
{
	return (0);
}

char	cb_fail(t_copt_parsed *opt)
{
	return (1);
}

char	vali(size_t argc, char **argv)
{
	while (argc--)
		strlen(argv[argc]);
	return (0);
}


char	vali_fail(size_t argc, char **argv)
{
	while (argc--)
		strlen(argv[argc]);
	return (1);
}

char	exec_test(t_copt copt, t_copt_parsed *res, const char **argv)
{
	size_t				i;

	i = 0;
	while (argv[i])
		i++;
	return copt_load(copt, res, i, argv);
}