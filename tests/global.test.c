/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   global.test.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fle-roy <francis.leroy@protonmail.ch>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/26 15:07:18 by fle-roy           #+#    #+#             */
/*   Updated: 2019/05/31 14:22:31 by fle-roy          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "copt.test.h"

Test(Global, opt_short_one)
{
	t_copt_parsed res;
	char			*argv[] = {"./a.out", "-t", NULL};

	t_copt_opt opt = (t_copt_opt){
		.sname = 't',
		.lname = "test",
		.mandatory = 0,
		.argc = 0,
		.validation = NULL,
	};
	t_copt_cmd gcmd = (t_copt_cmd){
		.text = NULL,
		.cb = &cb,
		.opt = (t_copt_opt*[]){&opt, NULL}
	};
	t_copt copt = (t_copt){
		.cmd = (t_copt_cmd*[]){&gcmd, NULL},
		.print = &printf
	};
	cr_assert(exec_test(copt, &res, (const char**)argv) == 0);
	cr_expect(res.argc == 2);
	cr_expect_str_eq(res.argv[0], argv[0]);
	cr_expect(res.gcmd == &gcmd);
	cr_expect(res.gopts[0] != NULL);
	cr_expect(res.gopts[0]->argc == 0);
	cr_expect(res.gopts[0]->argv[0] == 0);
	cr_expect(res.gopts[0]->times == 1);
	cr_expect(res.cmd == NULL);
	copt_free_parsed(&res);
}

Test(Global, opt_short_two)
{
	t_copt_parsed res;
	char			*argv[] = {"./a.out", "-t", "-y", NULL};

	t_copt_opt opt = (t_copt_opt){
		.sname = 't',
		.lname = "test",
		.mandatory = 0,
		.argc = 0,
		.validation = NULL,
	};
	t_copt_opt opt2 = (t_copt_opt){
		.sname = 'y',
		.lname = "testy",
		.mandatory = 0,
		.argc = 0,
		.validation = NULL,
	};
	t_copt_cmd gcmd = (t_copt_cmd){
		.text = NULL,
		.cb = &cb,
		.opt = (t_copt_opt*[]){&opt, &opt2, NULL}
	};
	t_copt copt = (t_copt){
		.cmd = (t_copt_cmd*[]){&gcmd, NULL},
		.print = &printf
	};
	cr_assert(exec_test(copt, &res, (const char**)argv) == 0);
	cr_expect(res.argc == 3);
	cr_expect_str_eq(res.argv[0], argv[0]);
	cr_expect(res.gcmd == &gcmd);
	cr_expect(res.gopts[0] != NULL);
	cr_expect(res.gopts[0]->argc == 0);
	cr_expect(res.gopts[0]->argv[0] == 0);
	cr_expect(res.gopts[0]->times == 1);
	cr_expect(res.gopts[1] != NULL);
	cr_expect(res.gopts[1]->argc == 0);
	cr_expect(res.gopts[1]->argv[0] == 0);
	cr_expect(res.gopts[1]->times == 1);
	cr_expect(res.cmd == NULL);
	copt_free_parsed(&res);
}

Test(Global, opt_long_one)
{
	t_copt_parsed res;
	char			*argv[] = {"./a.out", "--test", NULL};

	t_copt_opt opt = (t_copt_opt){
		.sname = 't',
		.lname = "test",
		.mandatory = 0,
		.argc = 0,
		.validation = NULL,
	};
	t_copt_cmd gcmd = (t_copt_cmd){
		.text = NULL,
		.cb = &cb,
		.opt = (t_copt_opt*[]){&opt, NULL}
	};
	t_copt copt = (t_copt){
		.cmd = (t_copt_cmd*[]){&gcmd, NULL},
		.print = &printf
	};
	cr_assert(exec_test(copt, &res, (const char**)argv) == 0);
	cr_expect(res.argc == 2);
	cr_expect_str_eq(res.argv[0], argv[0]);
	cr_expect(res.gcmd == &gcmd);
	cr_expect(res.gopts[0] != NULL);
	cr_expect(res.gopts[0]->argc == 0);
	cr_expect(res.gopts[0]->argv[0] == 0);
	cr_expect(res.gopts[0]->times == 1);
	cr_expect(res.cmd == NULL);
	copt_free_parsed(&res);
}

Test(Global, opt_long_two)
{
	t_copt_parsed res;
	char			*argv[] = {"./a.out", "--test", "--testy", NULL};

	t_copt_opt opt = (t_copt_opt){
		.sname = 't',
		.lname = "test",
		.mandatory = 0,
		.argc = 0,
		.validation = NULL,
	};
	t_copt_opt opt2 = (t_copt_opt){
		.sname = 'y',
		.lname = "testy",
		.mandatory = 0,
		.argc = 0,
		.validation = NULL,
	};
	t_copt_cmd gcmd = (t_copt_cmd){
		.text = NULL,
		.cb = &cb,
		.opt = (t_copt_opt*[]){&opt, &opt2, NULL}
	};
	t_copt copt = (t_copt){
		.cmd = (t_copt_cmd*[]){&gcmd, NULL},
		.print = &printf
	};
	cr_assert(exec_test(copt, &res, (const char**)argv) == 0);
	cr_expect(res.argc == 3);
	cr_expect_str_eq(res.argv[0], argv[0]);
	cr_expect(res.gcmd == &gcmd);
	cr_expect(res.gopts[0] != NULL);
	cr_expect(res.gopts[0]->argc == 0);
	cr_expect(res.gopts[0]->argv[0] == 0);
	cr_expect(res.gopts[0]->times == 1);
	cr_expect(res.gopts[1] != NULL);
	cr_expect(res.gopts[1]->argc == 0);
	cr_expect(res.gopts[1]->argv[0] == 0);
	cr_expect(res.gopts[1]->times == 1);
	cr_expect(res.cmd == NULL);
	copt_free_parsed(&res);
}

Test(Global, opt_short_repeat)
{
	t_copt_parsed res;
	char			*argv[] = {"./a.out", "-t", "-t", NULL};

	t_copt_opt opt = (t_copt_opt){
		.sname = 't',
		.lname = "test",
		.mandatory = 0,
		.argc = 0,
		.validation = NULL,
	};
	t_copt_cmd gcmd = (t_copt_cmd){
		.text = NULL,
		.cb = &cb,
		.opt = (t_copt_opt*[]){&opt, NULL}
	};
	t_copt copt = (t_copt){
		.cmd = (t_copt_cmd*[]){&gcmd, NULL},
		.print = &printf
	};
	cr_assert(exec_test(copt, &res, (const char**)argv) == 0);
	cr_expect(res.argc == 3);
	cr_expect_str_eq(res.argv[0], argv[0]);
	cr_expect(res.gcmd == &gcmd);
	cr_expect(res.gopts[0] != NULL);
	cr_expect(res.gopts[0]->argc == 0);
	cr_expect(res.gopts[0]->times == 2);
	cr_expect(res.cmd == NULL);
	copt_free_parsed(&res);
}

Test(Global, opt_short_repeat_concat)
{
	t_copt_parsed res;
	char			*argv[] = {"./a.out", "-tt", NULL};

	t_copt_opt opt = (t_copt_opt){
		.sname = 't',
		.lname = "test",
		.mandatory = 0,
		.argc = 0,
		.validation = NULL,
	};
	t_copt_cmd gcmd = (t_copt_cmd){
		.text = NULL,
		.cb = &cb,
		.opt = (t_copt_opt*[]){&opt, NULL}
	};
	t_copt copt = (t_copt){
		.cmd = (t_copt_cmd*[]){&gcmd, NULL},
		.print = &printf
	};
	cr_assert(exec_test(copt, &res, (const char**)argv) == 0);
	cr_expect(res.argc == 2);
	cr_expect_str_eq(res.argv[0], argv[0]);
	cr_expect(res.gcmd == &gcmd);
	cr_expect(res.gopts[0] != NULL);
	cr_expect(res.gopts[0]->argc == 0);
	cr_expect(res.gopts[0]->times == 2);
	cr_expect(res.cmd == NULL);
	copt_free_parsed(&res);
}

Test(Global, opt_long_repeat)
{
	t_copt_parsed res;
	char			*argv[] = {"./a.out", "--test", "--test", NULL};

	t_copt_opt opt = (t_copt_opt){
		.sname = 't',
		.lname = "test",
		.mandatory = 0,
		.argc = 0,
		.validation = NULL,
	};
	t_copt_cmd gcmd = (t_copt_cmd){
		.text = NULL,
		.cb = &cb,
		.opt = (t_copt_opt*[]){&opt, NULL}
	};
	t_copt copt = (t_copt){
		.cmd = (t_copt_cmd*[]){&gcmd, NULL},
		.print = &printf
	};
	cr_assert(exec_test(copt, &res, (const char**)argv) == 0);
	cr_expect(res.argc == 3);
	cr_expect_str_eq(res.argv[0], argv[0]);
	cr_expect(res.gcmd == &gcmd);
	cr_expect(res.gopts[0] != NULL);
	cr_expect(res.gopts[0]->argc == 0);
	cr_expect(res.gopts[0]->times == 2);
	cr_expect(res.cmd == NULL);
	copt_free_parsed(&res);
}

Test(Global, std_param)
{
	t_copt_parsed res;
	char			*argv[] = {"./a.out", "hello", NULL};

	t_copt_opt opt = (t_copt_opt){
		.sname = 't',
		.lname = "test",
		.mandatory = 0,
		.argc = 0,
		.validation = NULL,
	};
	t_copt_cmd gcmd = (t_copt_cmd){
		.text = NULL,
		.cb = &cb,
		.opt = (t_copt_opt*[]){&opt, NULL}
	};
	t_copt copt = (t_copt){
		.cmd = (t_copt_cmd*[]){&gcmd, NULL},
		.print = &printf
	};
	cr_assert(exec_test(copt, &res, (const char**)argv) == 0);
	cr_expect(res.argc == 2);
	cr_expect_str_eq(res.argv[0], argv[0]);
	cr_expect(res.gcmd == &gcmd);
	cr_expect(res.gopts[0] == NULL);
	cr_expect(res.cmd == NULL);
	cr_expect(res.params != NULL);
	cr_expect(res.params->argc == 1);
	cr_expect_str_eq(res.params->argv[0], argv[1]);
	copt_free_parsed(&res);
}

Test(Global, std_param_multiple)
{
	t_copt_parsed res;
	char			*argv[] = {"./a.out", "hello", "world", NULL};

	t_copt_opt opt = (t_copt_opt){
		.sname = 't',
		.lname = "test",
		.mandatory = 0,
		.argc = 0,
		.validation = NULL,
	};
	t_copt_cmd gcmd = (t_copt_cmd){
		.text = NULL,
		.cb = &cb,
		.opt = (t_copt_opt*[]){&opt, NULL}
	};
	t_copt copt = (t_copt){
		.cmd = (t_copt_cmd*[]){&gcmd, NULL},
		.print = &printf
	};
	cr_assert(exec_test(copt, &res, (const char**)argv) == 0);
	cr_expect(res.argc == 3);
	cr_expect_str_eq(res.argv[0], argv[0]);
	cr_expect(res.gcmd == &gcmd);
	cr_expect(res.gopts[0] == NULL);
	cr_expect(res.cmd == NULL);
	cr_expect(res.params != NULL);
	cr_expect(res.params->argc == 2);
	cr_expect_str_eq(res.params->argv[0], argv[1]);
	cr_expect_str_eq(res.params->argv[1], argv[2]);
	copt_free_parsed(&res);
}

Test(Global, std_param_double_dash)
{
	t_copt_parsed res;
	char			*argv[] = {"./a.out", "--", "hello", NULL};

	t_copt_opt opt = (t_copt_opt){
		.sname = 't',
		.lname = "test",
		.mandatory = 0,
		.argc = 0,
		.validation = NULL,
	};
	t_copt_cmd gcmd = (t_copt_cmd){
		.text = NULL,
		.cb = &cb,
		.opt = (t_copt_opt*[]){&opt, NULL}
	};
	t_copt copt = (t_copt){
		.cmd = (t_copt_cmd*[]){&gcmd, NULL},
		.print = &printf
	};
	cr_assert(exec_test(copt, &res, (const char**)argv) == 0);
	cr_expect(res.argc == 3);
	cr_expect_str_eq(res.argv[0], argv[0]);
	cr_expect(res.gcmd == &gcmd);
	cr_expect(res.gopts[0] == NULL);
	cr_expect(res.cmd == NULL);
	cr_expect(res.params != NULL);
	cr_expect(res.params->argc == 1);
	cr_expect_str_eq(res.params->argv[0], argv[2]);
	copt_free_parsed(&res);
}

Test(Global, std_param_double_dash_in_second_pos)
{
	t_copt_parsed res;
	char			*argv[] = {"./a.out", "hello1", "--", "hello", NULL};

	t_copt_opt opt = (t_copt_opt){
		.sname = 't',
		.lname = "test",
		.mandatory = 0,
		.argc = 0,
		.validation = NULL,
	};
	t_copt_cmd gcmd = (t_copt_cmd){
		.text = NULL,
		.cb = &cb,
		.opt = (t_copt_opt*[]){&opt, NULL}
	};
	t_copt copt = (t_copt){
		.cmd = (t_copt_cmd*[]){&gcmd, NULL},
		.print = &printf
	};
	cr_assert(exec_test(copt, &res, (const char**)argv) == 0);
	cr_expect(res.argc == 4);
	cr_expect_str_eq(res.argv[0], argv[0]);
	cr_expect(res.gcmd == &gcmd);
	cr_expect(res.gopts[0] == NULL);
	cr_expect(res.cmd == NULL);
	cr_expect(res.params != NULL);
	cr_expect(res.params->argc == 3);
	cr_expect_str_eq(res.params->argv[0], argv[1]);
	cr_expect_str_eq(res.params->argv[1], argv[2]);
	cr_expect_str_eq(res.params->argv[2], argv[3]);
	copt_free_parsed(&res);
}