/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   user_utils.test.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fle-roy <francis.leroy@protonmail.ch>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/26 15:05:41 by fle-roy           #+#    #+#             */
/*   Updated: 2019/05/31 14:42:01 by fle-roy          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "copt.test.h"

Test(UserUtils, Last_param_0)
{
	t_copt_parsed res;
	char			*argv[] = {"./a.out", "toto", "-t", "hello", NULL};

	t_copt_opt opt = (t_copt_opt){
		.sname = 't',
		.lname = "test",
		.mandatory = 0,
		.argc = 1,
		.validation = NULL,
	};
	t_copt_cmd cmd = (t_copt_cmd){
		.text = "toto",
		.cb = &cb,
		.opt = (t_copt_opt*[]){&opt, NULL}
	};
	t_copt copt = (t_copt){
		.cmd = (t_copt_cmd*[]){&cmd, NULL},
		.print = &printf
	};
	cr_assert(exec_test(copt, &res, (const char**)argv) == 0);
	cr_expect(res.argc == 4);
	cr_expect_str_eq(res.argv[0], argv[0]);
	cr_expect(res.params == NULL);
	cr_expect(res.cmd == &cmd);
	cr_expect(res.opts != NULL);
	cr_expect(res.opts[0]->argc == 1);
	cr_expect(res.opts[0]->times == 1);
	cr_expect_str_eq(res.opts[0]->argv[0], argv[3]);
	cr_expect(copt_get_last_opt(&res, 0).argc == 1);
	cr_expect(copt_get_last_opt(&res, 0).times == 1);
	cr_expect_str_eq(copt_get_last_opt(&res, 0).argv[0], argv[3]);
	copt_free_parsed(&res);
}

Test(UserUtils, Last_param_1)
{
	t_copt_parsed res;
	char			*argv[] = {"./a.out", "toto", "-t", "hello", "-t", "toto", NULL};

	t_copt_opt opt = (t_copt_opt){
		.sname = 't',
		.lname = "test",
		.mandatory = 0,
		.argc = 1,
		.validation = NULL,
	};
	t_copt_cmd cmd = (t_copt_cmd){
		.text = "toto",
		.cb = &cb,
		.opt = (t_copt_opt*[]){&opt, NULL}
	};
	t_copt copt = (t_copt){
		.cmd = (t_copt_cmd*[]){&cmd, NULL},
		.print = &printf
	};
	cr_assert(exec_test(copt, &res, (const char**)argv) == 0);
	cr_expect(res.argc == 6);
	cr_expect_str_eq(res.argv[0], argv[0]);
	cr_expect(res.params == NULL);
	cr_expect(res.cmd == &cmd);
	cr_expect(res.opts != NULL);
	cr_expect(res.opts[0]->argc == 1);
	cr_expect(res.opts[0]->times == 2);
	cr_expect(copt_get_last_opt(&res, 0).argc == 1);
	cr_expect(copt_get_last_opt(&res, 0).times == 2);
	cr_expect_str_eq(copt_get_last_opt(&res, 0).argv[0], argv[5]);
	copt_free_parsed(&res);
}

Test(UserUtils, GetMode_default)
{
	t_copt_parsed res;
	char			*argv[] = {"./a.out", "toto", NULL};

	t_copt_opt opt = (t_copt_opt){
		.sname = 't',
		.lname = "test",
		.mandatory = 0,
		.argc = 0,
		.validation = NULL,
	};
	t_copt_opt opt2 = (t_copt_opt){
		.sname = 'T',
		.lname = "TEST",
		.mandatory = 0,
		.argc = 0,
		.validation = NULL,
	};
	t_copt_cmd cmd = (t_copt_cmd){
		.text = "toto",
		.cb = &cb,
		.opt = (t_copt_opt*[]){&opt, &opt2, NULL}
	};
	t_copt copt = (t_copt){
		.cmd = (t_copt_cmd*[]){&cmd, NULL},
		.print = &printf
	};
	cr_assert(exec_test(copt, &res, (const char**)argv) == 0);
	cr_assert(copt_get_mode(&res, 0, 1, -1) == -1);
	copt_free_parsed(&res);
}

Test(UserUtils, GetMode_0)
{
	t_copt_parsed res;
	char			*argv[] = {"./a.out", "toto", "-ttttT", NULL};

	t_copt_opt opt = (t_copt_opt){
		.sname = 't',
		.lname = "test",
		.mandatory = 0,
		.argc = 0,
		.validation = NULL,
	};
	t_copt_opt opt2 = (t_copt_opt){
		.sname = 'T',
		.lname = "TEST",
		.mandatory = 0,
		.argc = 0,
		.validation = NULL,
	};
	t_copt_cmd cmd = (t_copt_cmd){
		.text = "toto",
		.cb = &cb,
		.opt = (t_copt_opt*[]){&opt, &opt2, NULL}
	};
	t_copt copt = (t_copt){
		.cmd = (t_copt_cmd*[]){&cmd, NULL},
		.print = &printf
	};
	cr_assert(exec_test(copt, &res, (const char**)argv) == 0);
	cr_assert(copt_get_mode(&res, 0, 1, -1) == 1);
	copt_free_parsed(&res);
}

Test(UserUtils, GetMode_1)
{
	t_copt_parsed res;
	char			*argv[] = {"./a.out", "toto", "-ttttTtttt", "-T", "-t", NULL};

	t_copt_opt opt = (t_copt_opt){
		.sname = 't',
		.lname = "test",
		.mandatory = 0,
		.argc = 0,
		.validation = NULL,
	};
	t_copt_opt opt2 = (t_copt_opt){
		.sname = 'T',
		.lname = "TEST",
		.mandatory = 0,
		.argc = 0,
		.validation = NULL,
	};
	t_copt_cmd cmd = (t_copt_cmd){
		.text = "toto",
		.cb = &cb,
		.opt = (t_copt_opt*[]){&opt, &opt2, NULL}
	};
	t_copt copt = (t_copt){
		.cmd = (t_copt_cmd*[]){&cmd, NULL},
		.print = &printf
	};
	cr_assert(exec_test(copt, &res, (const char**)argv) == 0);
	cr_assert(copt_get_mode(&res, 0, 1, -1) == 0);
	copt_free_parsed(&res);
}

Test(UserUtils, GetMode_with_args)
{
	t_copt_parsed res;
	char			*argv[] = {"./a.out", "toto", "-ttoto", "-T", "salut", NULL};

	t_copt_opt opt = (t_copt_opt){
		.sname = 't',
		.lname = "test",
		.mandatory = 0,
		.argc = 1,
		.validation = NULL,
	};
	t_copt_opt opt2 = (t_copt_opt){
		.sname = 'T',
		.lname = "TEST",
		.mandatory = 0,
		.argc = 1,
		.validation = NULL,
	};
	t_copt_cmd cmd = (t_copt_cmd){
		.text = "toto",
		.cb = &cb,
		.opt = (t_copt_opt*[]){&opt, &opt2, NULL}
	};
	t_copt copt = (t_copt){
		.cmd = (t_copt_cmd*[]){&cmd, NULL},
		.print = &printf
	};
	cr_assert(exec_test(copt, &res, (const char**)argv) == 0);
	cr_assert(copt_get_mode(&res, 0, 1, -1) == 1);
	copt_free_parsed(&res);
}