/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   copt.test.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fle-roy <francis.leroy@protonmail.ch>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/26 14:58:32 by fle-roy           #+#    #+#             */
/*   Updated: 2019/05/14 13:45:22 by fle-roy          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef COPT_TEST_H
# define COPT_TEST_H

# include <criterion/criterion.h>
# include "copt_internal.h"
# include "copt.h"
# include <stdio.h>

char	cb(t_copt_parsed *opt);
char	cb_fail(t_copt_parsed *opt);
char	vali(size_t argc, char **argv);
char	vali_fail(size_t argc, char **argv);
char	exec_test(t_copt copt, t_copt_parsed *res, const char **argv);

#endif