/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   llist.test.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fle-roy <francis.leroy@protonmail.ch>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/26 15:07:18 by fle-roy           #+#    #+#             */
/*   Updated: 2019/05/17 15:53:46 by fle-roy          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "copt.test.h"

Test(LList, CreateNewNode)
{
	t_copt_llist	*llist;

	llist = copt_llist_new((void*)1, (void*)2, 1235);
	cr_expect(llist != NULL);
	cr_expect(llist->prev == NULL);
	cr_expect(llist->next == NULL);
	cr_expect(llist->cmd == (void*)1);
	cr_expect(llist->opt == (void*)2);
	cr_expect(llist->index == 1235);
	copt_llist_destroy(&llist);
	cr_expect(llist == NULL);
}

Test(LList, CreateNewNode2)
{
	t_copt_llist	*llist;

	llist = copt_llist_new(NULL, NULL, 0);
	cr_expect(llist != NULL);
	cr_expect(llist->prev == NULL);
	cr_expect(llist->next == NULL);
	cr_expect(llist->cmd == NULL);
	cr_expect(llist->opt == NULL);
	copt_llist_destroy(&llist);
	cr_expect(llist == NULL);
}

Test(LList, AppendNodeBack)
{
	t_copt_llist	*llist;

	llist = copt_llist_new((void*)1, (void*)2, 0);
	copt_llist_push_back(&llist, (void*)3, (void*)4, 99);
	cr_expect(llist != NULL);
	cr_expect(llist->prev == NULL);
	cr_expect(llist->next != NULL);
	cr_expect(llist->cmd == (void*)1);
	cr_expect(llist->opt == (void*)2);
	cr_expect(llist->next->prev == llist);
	cr_expect(llist->next->next == NULL);
	cr_expect(llist->next->cmd == (void*)3);
	cr_expect(llist->next->opt == (void*)4);
	cr_expect(llist->next->index == 99);
	copt_llist_destroy(&llist);
	cr_expect(llist == NULL);
}

Test(LList, AppendNodeFront)
{
	t_copt_llist	*llist;

	llist = copt_llist_new((void*)1, (void*)2, 0);
	copt_llist_push_front(&llist, (void*)3, (void*)4, 88);
	cr_expect(llist != NULL);
	cr_expect(llist->prev != NULL);
	cr_expect(llist->next == NULL);
	cr_expect(llist->cmd == (void*)1);
	cr_expect(llist->opt == (void*)2);
	cr_expect(llist->prev->next == llist);
	cr_expect(llist->prev->prev == NULL);
	cr_expect(llist->prev->cmd == (void*)3);
	cr_expect(llist->prev->opt == (void*)4);
	cr_expect(llist->prev->index == 88);
	copt_llist_destroy(&llist);
	cr_expect(llist == NULL);
}

Test(LList, DeleteOne)
{
	t_copt_llist	*llist;

	llist = copt_llist_new((void*)1, (void*)2, 0);
	copt_llist_push_back(&llist, (void*)3, (void*)4, 0);
	copt_llist_del(&llist->next);
	cr_expect(llist != NULL);
	copt_llist_destroy(&llist);
	cr_expect(llist == NULL);
}

Test(LList, Destroy)
{
	t_copt_llist	*llist;

	llist = copt_llist_new((void*)1, (void*)2, 0);
	copt_llist_push_back(&llist, (void*)3, (void*)4, 0);
	copt_llist_destroy(&llist);
	cr_expect(llist == NULL);
}
