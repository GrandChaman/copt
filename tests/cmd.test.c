/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   cmd.test.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fle-roy <francis.leroy@protonmail.ch>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/26 15:00:46 by fle-roy           #+#    #+#             */
/*   Updated: 2019/05/31 14:05:16 by fle-roy          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "copt.test.h"

Test(Command, None)
{
	t_copt_parsed res;
	char			*argv[] = {"./a.out", NULL};
	t_copt copt = (t_copt){
		.cmd = (t_copt_cmd*[]){NULL},
		.print = &printf
	};
	cr_assert(exec_test(copt, &res, (const char**)argv) == -1);
	cr_expect(res.argc == 1);
	cr_expect_str_eq(res.argv[0], argv[0]);
	cr_expect(res.params == NULL);
	cr_expect(res.gcmd == NULL);
	cr_expect(res.cmd == NULL);
	cr_expect(res.opts == NULL);
	cr_expect(res.gopts == NULL);
	copt_free_parsed(&res);
}

Test(Command, One)
{
	t_copt_parsed res;
	char			*argv[] = {"./a.out", "toto", NULL};

	t_copt_cmd cmd = (t_copt_cmd){
		.text = "toto",
		.cb = &cb,
		.opt = (t_copt_opt*[]){NULL}
	};
	t_copt copt = (t_copt){
		.cmd = (t_copt_cmd*[]){&cmd, NULL},
		.print = &printf
	};
	cr_assert(exec_test(copt, &res, (const char**)argv) == 0);
	cr_expect(res.argc == 2);
	cr_expect_str_eq(res.argv[0], argv[0]);
	cr_expect(res.params == NULL);
	cr_expect(res.gcmd == NULL);
	cr_expect(res.cmd == &cmd);
	cr_expect(res.opts == NULL);
	cr_expect(res.gopts == NULL);
	copt_free_parsed(&res);
}

Test(Command, Two)
{
	t_copt_parsed res;
	char			*argv[] = {"./a.out", "toto", "toto2", NULL};

	t_copt_cmd cmd = (t_copt_cmd){
		.text = "toto",
		.cb = &cb,
		.opt = (t_copt_opt*[]){NULL}
	};

	t_copt_cmd cmd2 = (t_copt_cmd){
		.text = "toto2",
		.cb = &cb,
		.opt = (t_copt_opt*[]){NULL}
	};

	t_copt copt = (t_copt){
		.cmd = (t_copt_cmd*[]){&cmd, &cmd2, NULL},
		.print = &printf
	};
	cr_assert(exec_test(copt, &res, (const char**)argv) == 0);
	cr_expect(res.argc == 3);
	cr_expect_str_eq(res.argv[0], argv[0]);
	cr_expect(res.cmd == &cmd);
	copt_free_parsed(&res);
}

Test(Command, Wrong)
{
	t_copt_parsed res;
	char			*argv[] = {"./a.out", "toto", "totu", NULL};

	t_copt_cmd cmd = (t_copt_cmd){
		.text = "toto",
		.cb = &cb,
		.opt = (t_copt_opt*[]){NULL}
	};

	t_copt copt = (t_copt){
		.cmd = (t_copt_cmd*[]){&cmd, NULL},
		.print = &printf
	};
	cr_assert(exec_test(copt, &res, (const char**)argv) == 0);
	cr_expect(res.argc == 3);
	cr_expect_str_eq(res.argv[0], argv[0]);
	cr_expect(res.cmd == &cmd);
	copt_free_parsed(&res);
}