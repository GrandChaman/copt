/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   long_opt.test.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fle-roy <francis.leroy@protonmail.ch>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/26 15:05:41 by fle-roy           #+#    #+#             */
/*   Updated: 2019/05/31 14:18:32 by fle-roy          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "copt.test.h"


Test(Long_option, std)
{
	t_copt_parsed res;
	char			*argv[] = {"./a.out", "toto", "--test", NULL};

	t_copt_opt opt = (t_copt_opt){
		.sname = 't',
		.lname = "test",
		.mandatory = 0,
		.argc = 0,
		.validation = NULL,
	};
	t_copt_cmd cmd = (t_copt_cmd){
		.text = "toto",
		.cb = &cb,
		.opt = (t_copt_opt*[]){&opt, NULL}
	};
	t_copt copt = (t_copt){
		.cmd = (t_copt_cmd*[]){&cmd, NULL},
		.print = &printf
	};
	cr_assert(exec_test(copt, &res, (const char**)argv) == 0);
	cr_expect(res.argc == 3);
	cr_expect_str_eq(res.argv[0], argv[0]);
	cr_expect(res.params == NULL);
	cr_expect(res.cmd == &cmd);
	cr_expect(res.opts != NULL);
	cr_expect(res.opts[0]->argc == 0);
	cr_expect(res.opts[0]->times == 1);
	copt_free_parsed(&res);
}

Test(Long_option, std_n_arg)
{
	t_copt_parsed res;
	char			*argv[] = {"./a.out", "toto", "--test", "Hello", NULL};

	t_copt_opt opt = (t_copt_opt){
		.sname = 't',
		.lname = "test",
		.mandatory = 0,
		.argc = 1,
		.validation = NULL,
	};
	t_copt_cmd cmd = (t_copt_cmd){
		.text = "toto",
		.cb = &cb,
		.opt = (t_copt_opt*[]){&opt, NULL}
	};
	t_copt copt = (t_copt){
		.cmd = (t_copt_cmd*[]){&cmd, NULL},
		.print = &printf
	};
	cr_assert(exec_test(copt, &res, (const char**)argv) == 0);
	cr_expect(res.argc == 4);
	cr_expect_str_eq(res.argv[0], argv[0]);
	cr_expect(res.params == NULL);
	cr_expect(res.cmd == &cmd);
	cr_expect(res.opts != NULL);
	cr_expect(res.opts[0]->argc == 1);
	cr_expect(res.opts[0]->argv != NULL);
	cr_expect_str_eq(res.opts[0]->argv[0], argv[3]);
	cr_expect(res.opts[0]->times == 1);
	copt_free_parsed(&res);
}

Test(Long_option, std_n_arg_n_validation)
{
	t_copt_parsed res;
	char			*argv[] = {"./a.out", "toto", "--test", "Hello", NULL};

	t_copt_opt opt = (t_copt_opt){
		.sname = 't',
		.lname = "test",
		.mandatory = 0,
		.argc = 1,
		.validation = &vali,
	};
	t_copt_cmd cmd = (t_copt_cmd){
		.text = "toto",
		.cb = &cb,
		.opt = (t_copt_opt*[]){&opt, NULL}
	};
	t_copt copt = (t_copt){
		.cmd = (t_copt_cmd*[]){&cmd, NULL},
		.print = &printf
	};
	cr_assert(exec_test(copt, &res, (const char**)argv) == 0);
	cr_expect(res.argc == 4);
	cr_expect_str_eq(res.argv[0], argv[0]);
	cr_expect(res.params == NULL);
	cr_expect(res.cmd == &cmd);
	cr_expect(res.opts != NULL);
	cr_expect(res.opts[0]->argc == 1);
	cr_expect(res.opts[0]->argv != NULL);
	cr_expect_str_eq(res.opts[0]->argv[0], argv[3]);
	cr_expect(res.opts[0]->times == 1);
	copt_free_parsed(&res);
}

Test(Long_option, std_n_arg_n_validation_fail)
{
	t_copt_parsed res;
	char			*argv[] = {"./a.out", "toto", "--test", "Hello", NULL};

	t_copt_opt opt = (t_copt_opt){
		.sname = 't',
		.lname = "test",
		.mandatory = 0,
		.argc = 1,
		.validation = &vali_fail,
	};
	t_copt_cmd cmd = (t_copt_cmd){
		.text = "toto",
		.cb = &cb,
		.opt = (t_copt_opt*[]){&opt, NULL}
	};
	t_copt copt = (t_copt){
		.cmd = (t_copt_cmd*[]){&cmd, NULL},
		.print = &printf
	};
	cr_assert_not(exec_test(copt, &res, (const char**)argv) == 0);
}

Test(Long_option, std_n_args)
{
	t_copt_parsed res;
	char			*argv[] = {"./a.out", "toto", "--test", "Hello", "Hello2", NULL};

	t_copt_opt opt = (t_copt_opt){
		.sname = 't',
		.lname = "test",
		.mandatory = 0,
		.argc = 2,
		.validation = &vali,
	};
	t_copt_cmd cmd = (t_copt_cmd){
		.text = "toto",
		.cb = &cb,
		.opt = (t_copt_opt*[]){&opt, NULL}
	};
	t_copt copt = (t_copt){
		.cmd = (t_copt_cmd*[]){&cmd, NULL},
		.print = &printf
	};
	cr_assert(exec_test(copt, &res, (const char**)argv) == 0);
	cr_expect(res.argc == 5);
	cr_expect_str_eq(res.argv[0], argv[0]);
	cr_expect(res.params == NULL);
	cr_expect(res.cmd == &cmd);
	cr_expect(res.opts != NULL);
	cr_expect(res.opts[0]->argc == 2);
	cr_expect(res.opts[0]->argv != NULL);
	cr_expect_str_eq(res.opts[0]->argv[0], argv[3]);
	cr_expect_str_eq(res.opts[0]->argv[1], argv[4]);
	cr_expect(res.opts[0]->times == 1);
	copt_free_parsed(&res);
}

Test(Long_option, std_n_args_not_enough)
{
	t_copt_parsed res;
	char			*argv[] = {"./a.out", "toto", "--test", "Hello", NULL};

	t_copt_opt opt = (t_copt_opt){
		.sname = 't',
		.lname = "test",
		.mandatory = 0,
		.argc = 2,
		.validation = &vali,
	};
	t_copt_cmd cmd = (t_copt_cmd){
		.text = "toto",
		.cb = &cb,
		.opt = (t_copt_opt*[]){&opt, NULL}
	};
	t_copt copt = (t_copt){
		.cmd = (t_copt_cmd*[]){&cmd, NULL},
		.print = &printf
	};
	cr_assert_not(exec_test(copt, &res, (const char**)argv) == 0);
}

Test(Long_option, std_n_args_mandatory)
{
	t_copt_parsed res;
	char			*argv[] = {"./a.out", "toto", "--test", "Hello", NULL};

	t_copt_opt opt = (t_copt_opt){
		.sname = 't',
		.lname = "test",
		.mandatory = 1,
		.argc = 1,
		.validation = &vali,
	};
	t_copt_cmd cmd = (t_copt_cmd){
		.text = "toto",
		.cb = &cb,
		.opt = (t_copt_opt*[]){&opt, NULL}
	};
	t_copt copt = (t_copt){
		.cmd = (t_copt_cmd*[]){&cmd, NULL},
		.print = &printf
	};
	cr_assert(exec_test(copt, &res, (const char**)argv) == 0);
	cr_expect(res.argc == 4);
	cr_expect_str_eq(res.argv[0], argv[0]);
	cr_expect(res.params == NULL);
	cr_expect(res.cmd == &cmd);
	cr_expect(res.opts != NULL);
	cr_expect(res.opts[0]->argc == 1);
	cr_expect(res.opts[0]->argv != NULL);
	cr_expect_str_eq(res.opts[0]->argv[0], argv[3]);
	cr_expect(res.opts[0]->times == 1);
	copt_free_parsed(&res);
}

Test(Long_option, std_n_args_mandatory_missing)
{
	t_copt_parsed res;
	char			*argv[] = {"./a.out", "toto", "--test", NULL};

	t_copt_opt opt = (t_copt_opt){
		.sname = 't',
		.lname = "test",
		.mandatory = 1,
		.argc = 1,
		.validation = &vali,
	};
	t_copt_cmd cmd = (t_copt_cmd){
		.text = "toto",
		.cb = &cb,
		.opt = (t_copt_opt*[]){&opt, NULL}
	};
	t_copt copt = (t_copt){
		.cmd = (t_copt_cmd*[]){&cmd, NULL},
		.print = &printf
	};
	cr_assert_not(exec_test(copt, &res, (const char**)argv) == 0);
}

Test(Long_option, std_multiple_times)
{
	t_copt_parsed res;
	char			*argv[] = {"./a.out", "toto", "--test", "--test", NULL};

	t_copt_opt opt = (t_copt_opt){
		.sname = 't',
		.lname = "test",
		.mandatory = 0,
		.argc = 0,
		.validation = NULL,
	};
	t_copt_cmd cmd = (t_copt_cmd){
		.text = "toto",
		.cb = &cb,
		.opt = (t_copt_opt*[]){&opt, NULL}
	};
	t_copt copt = (t_copt){
		.cmd = (t_copt_cmd*[]){&cmd, NULL},
		.print = &printf
	};
	cr_assert(exec_test(copt, &res, (const char**)argv) == 0);
	cr_expect(res.argc == 4);
	cr_expect_str_eq(res.argv[0], argv[0]);
	cr_expect(res.params == NULL);
	cr_expect(res.cmd == &cmd);
	cr_expect(res.opts != NULL);
	cr_expect(res.opts[0]->argc == 0);
	cr_expect(res.opts[0]->argv != NULL);
	cr_expect(res.opts[0]->times == 2);
	copt_free_parsed(&res);
}

Test(Long_option, std_n_params_multiple_times)
{
	t_copt_parsed res;
	char			*argv[] = {"./a.out", "toto", "--test", "Toot", "--test", "Toot2", NULL};

	t_copt_opt opt = (t_copt_opt){
		.sname = 't',
		.lname = "test",
		.mandatory = 0,
		.argc = 1,
		.validation = NULL,
	};
	t_copt_cmd cmd = (t_copt_cmd){
		.text = "toto",
		.cb = &cb,
		.opt = (t_copt_opt*[]){&opt, NULL}
	};
	t_copt copt = (t_copt){
		.cmd = (t_copt_cmd*[]){&cmd, NULL},
		.print = &printf
	};
	cr_assert(exec_test(copt, &res, (const char**)argv) == 0);
	cr_expect(res.argc == 6);
	cr_expect_str_eq(res.argv[0], argv[0]);
	cr_expect(res.params == NULL);
	cr_expect(res.cmd == &cmd);
	cr_expect(res.opts != NULL);
	cr_expect(res.opts[0]->argc == 1);
	cr_expect(res.opts[0]->argv != NULL);
	cr_expect_str_eq(res.opts[0]->argv[0], argv[3]);
	cr_expect_str_eq(res.opts[0]->argv[1], argv[5]);
	cr_expect(res.opts[0]->times == 2);
	copt_free_parsed(&res);
}

Test(Long_option, std_double_dash)
{
	t_copt_parsed res;
	char			*argv[] = {"./a.out", "toto", "--test", "World", "--", "Hello", NULL};

	t_copt_opt opt = (t_copt_opt){
		.sname = 't',
		.lname = "test",
		.mandatory = 0,
		.argc = 2,
		.validation = NULL,
	};

	t_copt_cmd cmd = (t_copt_cmd){
		.text = "toto",
		.cb = &cb,
		.opt = (t_copt_opt*[]){&opt, NULL}
	};
	t_copt copt = (t_copt){
		.cmd = (t_copt_cmd*[]){&cmd, NULL},
		.print = &printf
	};
	cr_assert_not(exec_test(copt, &res, (const char**)argv) == 0);
}

Test(Long_option, std_unknown_opt)
{
	t_copt_parsed res;
	char			*argv[] = {"./a.out", "toto", "-v", "World", "Hello", NULL};

	t_copt_opt opt = (t_copt_opt){
		.sname = 't',
		.lname = "test",
		.mandatory = 0,
		.argc = 2,
		.validation = NULL,
	};

	t_copt_cmd cmd = (t_copt_cmd){
		.text = "toto",
		.cb = &cb,
		.opt = (t_copt_opt*[]){&opt, NULL}
	};
	t_copt copt = (t_copt){
		.cmd = (t_copt_cmd*[]){&cmd, NULL},
		.print = &printf
	};
	cr_assert(exec_test(copt, &res, (const char**)argv) != 0);
	copt_free_parsed(&res);
}

Test(Long_option, std_double_dash_before_opt)
{
	t_copt_parsed res;
	char			*argv[] = {"./a.out", "toto", "--", "--test", NULL};

	t_copt_opt opt = (t_copt_opt){
		.sname = 't',
		.lname = "test",
		.mandatory = 0,
		.argc = 0,
		.validation = NULL,
	};

	t_copt_cmd cmd = (t_copt_cmd){
		.text = "toto",
		.cb = &cb,
		.opt = (t_copt_opt*[]){&opt, NULL}
	};
	t_copt copt = (t_copt){
		.cmd = (t_copt_cmd*[]){&cmd, NULL},
		.print = &printf
	};
	cr_assert(exec_test(copt, &res, (const char**)argv) == 0);
	cr_expect(res.argc == 4);
	cr_expect_str_eq(res.argv[0], argv[0]);
	cr_expect(res.cmd == &cmd);
	cr_expect(res.opts != NULL);
	cr_expect(res.opts[0] == NULL);
	copt_free_parsed(&res);
}

Test(Long_option, std_whitespace_padded_opt)
{
	t_copt_parsed res;
	char			*argv[] = {"./a.out", "toto", "  \t\n\n\t--test", NULL};

	t_copt_opt opt = (t_copt_opt){
		.sname = 't',
		.lname = "test",
		.mandatory = 0,
		.argc = 0,
		.validation = NULL,
	};

	t_copt_cmd cmd = (t_copt_cmd){
		.text = "toto",
		.cb = &cb,
		.opt = (t_copt_opt*[]){&opt, NULL}
	};
	t_copt copt = (t_copt){
		.cmd = (t_copt_cmd*[]){&cmd, NULL},
		.print = &printf
	};
	cr_assert(exec_test(copt, &res, (const char**)argv) == 0);
	cr_expect(res.argc == 3);
	cr_expect_str_eq(res.argv[0], argv[0]);
	cr_expect(res.cmd == &cmd);
	cr_expect(res.opts != NULL);
	cr_expect(res.opts[0] != NULL);
	cr_expect(res.opts[0]->times == 1);
	cr_expect(res.opts[0]->argc == 0);
	copt_free_parsed(&res);
}

Test(Long_option, std_not_an_opt)
{
	t_copt_parsed res;
	char			*argv[] = {"./a.out", "toto", "   --    test", NULL};

	t_copt_opt opt = (t_copt_opt){
		.sname = 't',
		.lname = "test",
		.mandatory = 0,
		.argc = 0,
		.validation = NULL,
	};

	t_copt_cmd cmd = (t_copt_cmd){
		.text = "toto",
		.cb = &cb,
		.opt = (t_copt_opt*[]){&opt, NULL}
	};
	t_copt copt = (t_copt){
		.cmd = (t_copt_cmd*[]){&cmd, NULL},
		.print = &printf
	};
	cr_assert(exec_test(copt, &res, (const char**)argv) == 0);
	cr_expect(res.argc == 3);
	cr_expect_str_eq(res.argv[0], argv[0]);
	cr_expect(res.cmd == &cmd);
	cr_expect(res.opts[0] == NULL);
	copt_free_parsed(&res);
}

Test(Long_option, opt_cb_fail)
{
	t_copt_parsed res;
	char			*argv[] = {"./a.out", "toto", "--test", NULL};

	t_copt_opt opt = (t_copt_opt){
		.sname = 't',
		.lname = "test",
		.mandatory = 0,
		.argc = 0,
		.validation = NULL,
	};

	t_copt_cmd cmd = (t_copt_cmd){
		.text = "toto",
		.cb = &cb_fail,
		.opt = (t_copt_opt*[]){&opt, NULL}
	};
	t_copt copt = (t_copt){
		.cmd = (t_copt_cmd*[]){&cmd, NULL},
		.print = &printf
	};
	cr_assert(exec_test(copt, &res, (const char**)argv) != 0);
}