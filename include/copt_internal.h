/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   copt_internal.h                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fle-roy <francis.leroy@protonmail.ch>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/05 14:43:22 by fle-roy           #+#    #+#             */
/*   Updated: 2020/02/27 15:48:58 by fle-roy          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef COPT_INTERNAL_H
# define COPT_INTERNAL_H

# include <stdlib.h>
# include <unistd.h>
# include <errno.h>
# include "copt.h"
# include <stdint.h>
# define COPT_SEARCHING_OPT		1
# define COPT_SEARCHING_OPT_ARG	2

/*
 ** MEM & STR
*/

void				*copt_malloc(size_t len);
void				*copt_realloc(void *old, size_t oldlen, size_t len);
void				copt_free(void **ptr);
void				*copt_memcpy(void *dest, void *src, size_t count);
size_t				copt_strlen(char *str);
char				*copt_strsub(char *s, size_t start, size_t len);
char				copt_strcmp(const char *lhs, const char *rhs);
char				*copt_trim(char *s);
char				copt_is_in_opt_charset(const char *s);
void				*copt_memset(void *dest, unsigned char c, size_t count);

/*
 ** LLIST
*/

t_copt_llist		*copt_llist_new(t_copt_cmd *cmd, t_copt_parsed_opt *opt,
	size_t index);
void				copt_llist_del(t_copt_llist **alst);
void				copt_llist_destroy(t_copt_llist **list);
uint8_t				copt_llist_push_back(t_copt_llist **begin_list,
	t_copt_cmd *cmd, t_copt_parsed_opt *opt, size_t index);
uint8_t				copt_llist_push_front(t_copt_llist **begin_list,
	t_copt_cmd *cmd, t_copt_parsed_opt *opt, size_t index);

/*
 ** CMD
*/

t_copt_cmd			*copt_find_cmd(t_copt copt, char *search);

/*
 ** OPT
*/

size_t				copt_opt_nb_in_cmd(t_copt_cmd *cmd);
t_copt_opt			*copt_find_short_opt(t_copt_cmd *cmd, char search);
t_copt_opt			*copt_find_long_opt(t_copt_cmd *cmd, char *search);
t_copt_parsed_opt	*copt_init_parsed_opt(t_copt_opt *opt);
t_copt_parsed_opt	**copt_init_parsed_opts(t_copt_cmd *cmd);
uint8_t				copt_enlarge_parsed_opt(t_copt_parsed_opt *opt);
char				copt_is_ddash(char *str);
char				copt_insert_opt(t_copt_parsed *res, t_copt_cmd *cmd,
	t_copt_parsed_opt **po, t_copt_opt *opt);
char				*copt_is_long_opt(const char *opt);
char				*copt_is_short_opt(const char *opt);
char				copt_read_short_opt(t_copt copt, t_copt_parsed *res,
	t_copt_args *args, char *opt);
char				copt_read_long_opt(t_copt copt, t_copt_parsed *res,
	t_copt_args *args, char *opt);
char				copt_opt_read_args(t_copt_parsed_opt *res,
	char **actual, t_copt_args *args);
char				copt_opt_validation(t_copt_parsed_opt **res,
	t_copt_opt *opt);

char				copt_opt_error_validation(t_copt copt, t_copt_opt *opt);
char				copt_opt_error_args(t_copt copt, t_copt_opt *opt);
char				copt_opt_doesnt_exists(t_copt copt, char *opt,
	t_copt_cmd *cmd);

/*
 ** PIPELINE
*/

char				*copt_consume_arg(t_copt_args *args);
char				*copt_restore_arg(t_copt_args *args);
char				copt_parsed_init(t_copt copt, t_copt_parsed *res,
	t_copt_args *args);
char				copt_pipeline_routine(t_copt copt, t_copt_parsed *res,
	t_copt_args *args);

#endif
