/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   copt.h                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fle-roy <francis.leroy@protonmail.ch>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/05 14:00:15 by bluff             #+#    #+#             */
/*   Updated: 2019/09/10 18:22:25 by fle-roy          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef COPT_H
# define COPT_H
# include <stdlib.h>

typedef struct				s_copt_parsed_opt
{
	size_t					id; // The id of this option, should be unique
	size_t					times; // The number of times it has been detected
	size_t					argc; // The number of arguments in each array
	char					**argv; // Array of arguments
}							t_copt_parsed_opt;

typedef struct				s_copt_opt
{
	char					sname; // Short name : '-e'
	char					*lname; // Long name : '--edit'
	char					mandatory; // If != 0 then it's a mandatory argument
	size_t					argc; // Number of args needed
	char					(*validation)(size_t, char**); // A validation function, if return != 0 -> abort
	size_t					id; // The unique id of this opt
	char					*desc; // The description of this argument
}							t_copt_opt;

struct s_copt_parsed;

typedef struct				s_copt_cmd
{
	char					*text; // The command : "edit", `null` for global command
	t_copt_opt				**opt; // An array of option accepted by this command
	char					(*cb)(struct s_copt_parsed*); // A callback upon the call of this function
	char					*desc; // A description of this command
}							t_copt_cmd;

typedef struct				s_copt
{
	t_copt_cmd				**cmd; // An array of accepted command
	int						(*print)(const char *ptrn, ...); // The print function to use (should be `printf` like)
	char					*prgm_name; // The name of the programm
	char					*version; // The version of the program
	char					*desc; // A description of the program
}							t_copt;

typedef struct				s_copt_llist
{
	t_copt_cmd				*cmd; // The command called
	t_copt_parsed_opt		*opt; // The option in this node
	size_t					index; // The node index
	struct s_copt_llist		*next; // The next node's address
	struct s_copt_llist		*prev; // The previous node's address
}							t_copt_llist;

typedef struct				s_copt_parsed
{
	const char				**argv; // The arguments (everyone of them)
	size_t					argc; // The number of arguments (everyone of them)
	t_copt_cmd				*cmd; // The called command (if applicable)
	t_copt_cmd				*gcmd; // The global command (if applicable)
	t_copt_parsed_opt		**opts; // The called command's parsed options (if applicable)
	t_copt_parsed_opt		**gopts; // The called global command's parsed options (if applicable)
	t_copt_parsed_opt		*params; // The left over parameters
	t_copt_llist			*list; // The linked list of parsed parameters
}							t_copt_parsed;

typedef struct				s_copt_args
{
	size_t					argc;
	const char				**argv;
}							t_copt_args;

/**
 * @brief Load the copt object, parse the command line arguments and execute callbacks
 * 
 * @param copt The copt configuration structure
 * @param res The pointer to the result object
 * @param argc The number of arguments
 * @param argv An array of `argc` arguments
 * @return char 0 on success, != 0 otherwise
 */
char						copt_load(t_copt copt, t_copt_parsed *res,
	size_t argc, const char **argv);

/**
 * @brief Free the result structure
 * 
 * @param res The result structure
 */
void						copt_free_parsed(t_copt_parsed *res);

/**
 * @brief Display help message with the defined `print` function
 * 
 * @param copt The configuration structure
 */
void						copt_display_help(t_copt copt);

/**
 * @brief Iterate on the linked list of results
 * 
 * @param copt The configuration structure
 * @param lst The linked list first node
 * @param f The function to call on each node
 * @return char 0 on success, != 0 otherwise
 */
char						copt_llist_iter(t_copt copt, t_copt_llist *lst,
	char (*f)(t_copt, t_copt_llist *lst));

/**
 * @brief Get the last opt's argument list (the one that really count) for a
 * specific argument
 * 
 * @param parsed The result structure
 * @param opt The option to get the argument list for
 * @return t_copt_parsed_opt The argument list
 */
t_copt_parsed_opt			copt_get_last_opt(t_copt_parsed *parsed,
	size_t opt);

/**
 * @brief Get which mode is activated if 2 options works as XOR state defined
 * If the 2 options are present, only the last one will decide which mode is activated
 * 
 * @param parsed The result structure
 * @param opt1 Option 1's id
 * @param opt2 Option 2's id
 * @param dft The default value if neither is actived
 * @return size_t The selected option's id or the default value if none are activated
 */
size_t						copt_get_mode(t_copt_parsed *parsed,
	size_t opt1, size_t opt2, size_t dft);
#endif
